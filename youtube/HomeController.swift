//
//  ViewController.swift
//  youtube
//
//  Created by Sanauyar Ali on 11/12/16.
//  Copyright © 2016 Ali Corporation. All rights reserved.
//

import UIKit

class HomeController: UICollectionViewController,UICollectionViewDelegateFlowLayout
{
    let cellId = "cellId"
    let titles = ["Home","Trending","Subscriptions","Account"]
   
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = false;
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width-32, height: view.frame.height));
        titleLabel.textColor = UIColor.white;
        titleLabel.font = UIFont.systemFont(ofSize: 20);
        titleLabel.text = " Home";
        navigationItem.titleView = titleLabel;
        //VideoCell registration
        setupCollectionView();
        setupMenuBar();
        setupNavBarButtons();

    }
    func setupCollectionView()
    {
        if let flowLayout = collectionView?.collectionViewLayout as?UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal;
            flowLayout.minimumLineSpacing = 0;
        }
        collectionView?.backgroundColor = UIColor.white;
        collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: cellId);
        collectionView?.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0);
        collectionView?.isPagingEnabled = true
    }

   func setupNavBarButtons()
    {
        let searchImage = UIImage(named: "search_icon.png")?.withRenderingMode(.alwaysOriginal);
        let searchBarButtonItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch));
        let moreButton = UIBarButtonItem(image: UIImage(named: "nav_more_icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMore));
        navigationItem.rightBarButtonItems = [moreButton,searchBarButtonItem];
    }
    func handleSearch()
    {
       // print("123");
        scrollToMenuIndex(menuIndex: 2);
    }
    func scrollToMenuIndex(menuIndex:Int)
    {
        let indexPath = NSIndexPath(item: menuIndex, section: 0);
        collectionView?.scrollToItem(at: indexPath as IndexPath, at:.init(rawValue: 0), animated: true);
        setTitleForIndex(index: menuIndex);
    }

    private func setTitleForIndex(index:Int)
    {
        if let titleLabel =  navigationItem.titleView as? UILabel {
            titleLabel.text = "  \(titles[index])";
        }
    }
    lazy var settingsLauncher :SettingsLauncher = {
        let launcher = SettingsLauncher();
        launcher.homeController = self
        return launcher;
    }()

    func handleMore()
    {
      settingsLauncher.ShowSettings();

    }
    func showControllerForSetting(setting:Setting)
     {
        let dummySettingsViewController = UIViewController();
        dummySettingsViewController.view.backgroundColor = UIColor.white;
        dummySettingsViewController.navigationItem.title = setting.name.rawValue;
  navigationController?.navigationBar.titleTextAttributes =      [NSForegroundColorAttributeName: UIColor.white];
        navigationController?.navigationBar.tintColor = UIColor.white;
        navigationController?.pushViewController(dummySettingsViewController, animated: true);

    }
lazy var menuBar: MenuBar =
    {
        let mb = MenuBar()
        mb.homeController = self;
        return mb;
    }();
    private func setupMenuBar()
    {
        let redView = UIView();
        redView.backgroundColor = UIColor.rgb(red: 230, green: 32, blue: 31);
        view.addSubview(redView);
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: redView);
        view.addConstraintsWithFormat(format: "V:[v0(50)]", views: redView);
        navigationController?.hidesBarsOnSwipe = true;
        view.addSubview(menuBar);
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: menuBar);
        view.addConstraintsWithFormat(format: "V:[v0(50)]", views: menuBar);
        menuBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true;
    }

    //scrolling the white view with section
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.x);
        menuBar.horizontalBarLetfAncharConstraint?.constant = scrollView.contentOffset.x/4;
    }

    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
       let index = targetContentOffset.pointee.x/view.frame.width;
        let indexPath = NSIndexPath(item: Int(index), section: 0);
        menuBar.collectionView.selectItem(at: indexPath as IndexPath, animated: true, scrollPosition: .init(rawValue: 0));
        setTitleForIndex(index: Int(index));
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4;
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath);
        return cell;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - 50);
    }
}
















