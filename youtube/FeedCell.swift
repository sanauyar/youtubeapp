//
//  FeedCell.swift
//  youtube
//
//  Created by Sanauyar Ali on 30/12/16.
//  Copyright © 2016 Ali Corporation. All rights reserved.
//

import UIKit

class FeedCell: BaseCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextViewDelegate {
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.dataSource = self;
        cv.delegate = self;
        return cv;
    }()
    var videos: [Video] = {
        var KanyeChannel = Channel();
        KanyeChannel.name = "let us learn together";
        KanyeChannel.profileImageName = "kanye_profile";
        var blankSpaceVideo = Video();
        blankSpaceVideo.title = "Ali an iOS Developer";
        blankSpaceVideo.thumbnailImage = "IMG_4963.JPG";
        blankSpaceVideo.channel = KanyeChannel;
        blankSpaceVideo.numberOfViews = 236786865454 as NSObject?;
        var badbloodVideo = Video();
        badbloodVideo.title = "XCODE 8 using iOS 10 version of app development";
        badbloodVideo.thumbnailImage = "XCDODE 8 New.JPG"
        badbloodVideo.channel = KanyeChannel;
        badbloodVideo.numberOfViews = 5076786876 as NSObject?;
        return [blankSpaceVideo,badbloodVideo];
    }()
    func  fetchVideos()
    {   let url:NSURL? = NSURL(fileURLWithPath: "home.json");
        //let  url = NSURL(string: "url");
        URLSession.shared.dataTask(with: url! as URL)
        {
            (data,response,error) in
            if (error != nil)
            {
                print(error!);
                return;

            }
            do
            {   let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers);
                for dictionary in json as![[String:AnyObject]]
                {
                    print(dictionary["title"]as! String);
                }
            }
            catch let jsonError
            {
                print(jsonError);
            }
            }.resume();
    }

    let cellId = "cellId";
    override func setupViews() {
        super.setupViews();
        fetchVideos();
        backgroundColor = .brown
        addSubview(collectionView);
        addConstraintsWithFormat(format:"H:|[v0]|",views: collectionView);
        addConstraintsWithFormat(format: "V:|[v0]|", views: collectionView);
        collectionView.register(VideoCell.self, forCellWithReuseIdentifier: cellId)
    }
             func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return videos.count;
        }
    
         func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! VideoCell;
            cell.video = videos[indexPath.item];
            //cell.backgroundColor = UIColor.red;
            return cell;
        }
        public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
        {
            let height = (frame.width-16-16)*9/16;
            return CGSize(width: frame.width, height: height+16+90);
        }
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
        {
            return 0;
        }


}
