//  VideoCell.swift
//  youtube
//
//  Created by Sanauyar Ali on 12/12/16.
//  Copyright © 2016 Ali Corporation. All rights reserved.
//

import UIKit

class BaseCell:UICollectionViewCell{
    override init(frame:CGRect)
    {
        super.init(frame: frame)
        setupViews();
    }
    func setupViews()
    {

    }
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }

}
class VideoCell:BaseCell
{
    var video : Video?{
        didSet{
            titleLabel.text = video?.title;
            titleLabel.numberOfLines = 2;
            thumbnailImageView.image = UIImage(named: (video?.thumbnailImage)!);
            if let profileImageName = video?.channel?.profileImageName{
                userProfileImageView.image = UIImage(named: profileImageName);
            }
            let numberFormatter = NumberFormatter();
            numberFormatter.numberStyle = .decimal;
            if let channelName = video?.channel?.name,let numberOfViews = video?.numberOfViews {
                let subTitleText = "\(channelName) .\(numberFormatter.string(from: numberOfViews as! NSNumber)!)views - 2 years ago";
                subTitleTextView.text = subTitleText;
            }
           //measure title text
            if let title = video?.title{
                let size = CGSize(width: frame.width-16-44-8-16, height: 1000);
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin);
                let estimatedRect = NSString(string: title).boundingRect(with: size, options: options, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 14)], context: nil);
                if estimatedRect.size.height > 20 {
                    titleLabelHeight?.constant = 44;
                }
                else{
                    titleLabelHeight?.constant = 20;
                }
            }
        }
    }
    let thumbnailImageView:UIImageView =
        {
            let imageView = UIImageView();
            //imageView.backgroundColor = UIColor.blue;
            imageView.image = UIImage(named: "IMG_4963.JPG");
            imageView.contentMode = .scaleAspectFill;
            imageView.clipsToBounds = true;
            return imageView;
    }();
    let userProfileImageView :UIImageView =
        {
            let imageView = UIImageView();
            //imageView.backgroundColor = UIColor.green;
            imageView.image = UIImage(named: "aliimg.jpg");
            imageView.layer.cornerRadius = 22;
            imageView.layer.masksToBounds = true;
            return imageView;
    }();
    let seprateView: UIView =
        {
            let view = UIView();
            view.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230);
            //view.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1);
            return view;
    }();

    let titleLabel: UILabel =
        {
            let label = UILabel();
            //label.backgroundColor = UIColor.purple;
            label.translatesAutoresizingMaskIntoConstraints = false;
            label.text = "Ali an iOS Developer";
            return label;
    }();
    let subTitleTextView: UITextView =
        {
            let textView = UITextView();
            //textView.backgroundColor = UIColor.brown;
            textView.translatesAutoresizingMaskIntoConstraints = false;
            textView.text = "You can visit our youtube chanell,Go with Natural Tricks for more tutorials";
            textView.isUserInteractionEnabled = false;
            //This line of code is used to up the text by 4 picksal up
            //textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0);
            textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0);
            textView.textColor = UIColor.gray;
            return textView;
    }();
    var titleLabelHeight:NSLayoutConstraint?;
    override func setupViews()
    {
        addSubview(thumbnailImageView);
        addSubview(seprateView);
        addSubview(userProfileImageView);
        addSubview(titleLabel);
        addSubview(subTitleTextView);
        //Horizontal constraint for thumbnail Image View
        addConstraintsWithFormat(format:"H:|-16-[v0]-16-|" , views: thumbnailImageView)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: seprateView);
        //Horizontal constraint for userProfile Image View
        addConstraintsWithFormat(format: "H:|-16-[v0(44)]|", views: userProfileImageView);
        //Verical constraint all
        addConstraintsWithFormat(format: "V:|-16-[v0]-8-[v1(44)]-28-[v2(1)]|", views: thumbnailImageView,userProfileImageView,seprateView);
        // add top constraint to title label
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: thumbnailImageView, attribute: .bottom, multiplier: 1, constant: 8));
        //letf constraint
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .left, relatedBy: .equal, toItem: userProfileImageView, attribute: .right, multiplier: 1, constant: 8));
        //right constraint
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .right, relatedBy: .equal, toItem: thumbnailImageView, attribute: .right, multiplier: 1, constant: 0));
        //height constraint
        titleLabelHeight = NSLayoutConstraint(item: titleLabel, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0, constant: 44);
        addConstraint(titleLabelHeight!);

        // add top constraint to Subtitle label
        addConstraint(NSLayoutConstraint(item: subTitleTextView, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 4));
        //letf constraint
        addConstraint(NSLayoutConstraint(item: subTitleTextView, attribute: .left, relatedBy: .equal, toItem: userProfileImageView, attribute: .right, multiplier: 1, constant: 8));
        //right constraint
        addConstraint(NSLayoutConstraint(item: subTitleTextView, attribute: .right, relatedBy: .equal, toItem: thumbnailImageView, attribute: .right, multiplier: 1, constant: 0));
        //height constraint
        addConstraint(NSLayoutConstraint(item: subTitleTextView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0, constant: 30));
    }

}

