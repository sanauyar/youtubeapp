//
//  Video.swift
//  youtube
//
//  Created by Sanauyar Ali on 16/12/16.
//  Copyright © 2016 Ali Corporation. All rights reserved.
//

import UIKit
class Video:NSObject{
    var thumbnailImage:String?;
    var title:String?;
    var channel:Channel?;
    var numberOfViews:NSObject?;
    var uploadDate:NSDate?;
}

class Channel:NSObject {
    var name:String?
    var profileImageName:String?;
}
