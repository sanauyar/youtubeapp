//
//  SettingsCell.swift
//  youtube
//
//  Created by Sanauyar Ali on 23/12/16.
//  Copyright © 2016 Ali Corporation. All rights reserved.
//

import UIKit
class SettingCell: BaseCell {

    override var isHighlighted: Bool{
        didSet{
            backgroundColor = isHighlighted ? UIColor.darkGray : UIColor.white;
           // print(isHighlighted);
            iconImageView.tintColor = isHighlighted ? UIColor.white : UIColor.black;
            nameLabel.tintColor = isHighlighted ? UIColor.white: UIColor.black
        }
    }

    var setting: Setting? {
        didSet{
            nameLabel.text = (setting?.name).map { $0.rawValue };
            if let imageName = setting?.imageName{
                iconImageView.image = UIImage(named:imageName)?.withRenderingMode(.alwaysTemplate);
                iconImageView.tintColor = UIColor.darkGray;
            }
        }
    }

    let nameLabel:UILabel = {
        let label = UILabel();
        label.text = "Setting";
        //label.textAlignment = .center;
        label.font = UIFont.systemFont(ofSize: 13);
        return label;
    }();
    let iconImageView:UIImageView = {
        let imageView = UIImageView();
        imageView.image = UIImage(named: "settings");
        imageView.contentMode = .scaleAspectFill;
        return imageView;
        }();
    override func setupViews() {
        super.setupViews();
        //backgroundColor = UIColor.blue;
        addSubview(nameLabel);
        addSubview(iconImageView);
        addConstraintsWithFormat(format: "H:|-8-[v0(30)]-8-[v1]|", views: iconImageView,nameLabel);
        addConstraintsWithFormat(format: "V:|[v0]|", views: nameLabel);
        addConstraintsWithFormat(format: "V:[v0(30)]", views: iconImageView);
        addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0));
    }
//    override init()
//    {
//        super.init()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
}
